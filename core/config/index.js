const fs = require('fs');
const nconf = require('nconf');
const path = require('path');
const configFilePath = path.resolve(__dirname, "config.json");

// 
// Setup nconf to use (in-order): 
//   1. Command-line arguments 
//   2. Environment variables 
//   3. A file located at './config.json' 
// 

nconf.argv()
  .env()
  .file({ file: configFilePath });


global.nconf = nconf;
//module.exports = nconf;
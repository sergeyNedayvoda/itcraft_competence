'use strict';

const express = require('express');
const path = require('path');
require('./core/config/index');

// Constants
const PORT = nconf.get("port");
const app = express();

const indexFilePath = path.resolve(__dirname, "view/pages/index.ejs"); 

// App
// setup ejs 
app.set('view engine', 'ejs');

app.use("/public", express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.render(indexFilePath);
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT); 